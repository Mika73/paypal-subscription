<?php

use Service\Db;
use Paypal\Paypal;
use Service\DotEnv;
use Renderer\Render;

require_once 'Class/autoload.php';

$dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
$paypal = new Paypal($dotEnv['CLIENT_ID'], $dotEnv['CLIENT_SECRET'], $dotEnv['IS_PROD']);
$db = new Db();

// Create subscription
if (isset($_GET['id'])) {

  $location = $_SERVER['HTTP_HOST'];

  if( substr($location, 0, 9) ==='localhost'){
    $url = "http://$location/createSubscription.php";
  }else{
    $url = "https://$location/createSubscription.php";
  }

  $subscriptionData = new stdClass();
  $subscriptionData->plan_id = $_GET['id'];

  $application_context = new stdClass();
  $application_context->cancel_url = $url.'?success=false';
  $application_context->return_url = $url.'?success=true';

  $subscriptionData->application_context = $application_context;
  $subscriptionData->auto_renewal = true;

  $resp = $paypal->createSubscription($subscriptionData);
  $db->createSubscription($resp->id, $resp->status);

  foreach ($resp->links as $link) {
    if ($link->rel === 'approve') {
      header("Location:$link->href");
      break;
    }
  }
}

// Paypal subscription completed. Display success or error message
if (isset($_GET['success']) && $_GET['success']=== 'true') {
  $_SESSION['flash_message'] = "Payment was successfully completed";
}
if (isset($_GET['success']) && $_GET['success']=== 'false') {
  $_SESSION['flash_err_message'] = "Payment failed";
}

// Handle Paypal webhook
if (isset($_GET['webhook']) && $_GET['webhook']=== 'true') {

  $json_str = file_get_contents('php://input');
  $event = json_decode($json_str);

  $resp = $paypal->handleWebhook($event, $dotEnv['WEBHOOK_ID']);
  if($resp === 'PAYMENT.SALE.COMPLETED'){
    $db->updateSubscription($event->resource->billing_agreement_id, $resp, json_encode($event->resource));
  }
  if($resp === 'BILLING.SUBSCRIPTION.CANCELLED'){
    $db->updateSubscription($event->resource->id, $resp, json_encode($event->resource));
  }
  if($resp === 'BILLING.SUBSCRIPTION.PAYMENT.FAILED'){
    $db->updateSubscription($event->resource->id, $resp, json_encode($event->resource));
  }
}

// Fetch all plans to display
$planList = $paypal->findAllPlan();

?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Paypal TEST</h1>
    <hr>
    <h2>Subscription</h2>
    <?= Render::flashMessage() ?>
    <table class="table table-striped">
      <tr>
        <th>Plan ID</th>
        <th>Product ID</th>
        <th>Plan Name</th>
        <th>Status</th>
        <th>Create at</th>
        <th>Action</th>
      </tr>
      <?php
      foreach ($planList->plans as $pl) {
        echo "
        <tr>
          <td>$pl->id</td>
          <td>$pl->product_id</td>
          <td>$pl->name</td>
          <td>$pl->status</td>
          <td>$pl->create_time</td>";
        if ($pl->status === 'ACTIVE') {
          echo "<td><a href='createSubscription.php?id=$pl->id'>Subscribe</a></td></tr>";
        } else {
          echo "<td></td></tr>";
        }
      }
      ?>
    </table>
    <div class="mb-5">
      <a href="/">Back</a>
    </div>
  </div>
</body>

</html>
