<?php

namespace Paypal;
class Api
{
  public static function callApi($accessToken, $url, $requestMethod, $data)
  {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    if ($requestMethod === 'POST') {
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }

    if ($requestMethod === 'DELETE') {
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    }

    $headers = array(
      "Authorization: Bearer $accessToken",
      "Content-Type: application/json",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    //for debug only!
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);
    $resp = json_decode($resp);

    return $resp;
  }
}
