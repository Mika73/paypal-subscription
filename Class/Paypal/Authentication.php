<?php

namespace Paypal;

class Authentication
{
  private const PAYPAL_AUTH_PATH = 'oauth2/token';
  private $url;

  public function __construct($rootUrl) {
    $this->url = $rootUrl.self::PAYPAL_AUTH_PATH;
  }

  public function getToken($clientId, $clientSecret)
  {
    $encode64 = base64_encode("$clientId:$clientSecret");
    $curl = curl_init($this->url);
    curl_setopt($curl, CURLOPT_URL, $this->url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
      "Authorization: Basic $encode64",
      "Content-Type: application/x-www-form-urlencoded",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    $data = "grant_type=client_credentials";

    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    //for debug only!
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    // file_put_contents("assets/log/auth.json",$resp, FILE_APPEND);
    $resp = json_decode($resp);

    if (isset($resp->access_token)) {
      return $resp->access_token;
    }

    return null;
  }
}
