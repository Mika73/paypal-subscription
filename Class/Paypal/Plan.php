<?php

namespace Paypal;

use Paypal\Api;

class Plan extends Model
{
  private const PAYPAL_PLANS_PATH = 'billing/plans';
  protected $url;

  public function __construct($rootUrl, $accessToken) {
    $this->url = $rootUrl.self::PAYPAL_PLANS_PATH;
    $this->accessToken = $accessToken;
  }

  public function activate($id, $activate){

    if($activate === true){
      $url = "$this->url/$id/activate";
    }else{
      $url = "$this->url/$id/deactivate";
    }
    Api::callApi($this->accessToken, $url, 'POST', null);
  }
}
