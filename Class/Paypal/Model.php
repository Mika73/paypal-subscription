<?php

namespace Paypal;

use Paypal\Api;

abstract class Model
{
  protected $accessToken;
  protected $url;

  public function findAll()
  {
    $resp = Api::callApi($this->accessToken, $this->url.'?page=1&page_size=20', 'GET', null);
    return $resp;
  }

  public function create($data)
  {
    $resp = Api::callApi($this->accessToken, $this->url, 'POST', json_encode($data));
    return $resp;
  }

  public function show($id)
  {
    $url = "$this->url/$id";
    $resp = Api::callApi($this->accessToken, $url, 'GET', null);
    return $resp;
  }
}
