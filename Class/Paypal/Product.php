<?php

namespace Paypal;

class Product extends Model
{
  private const PAYPAL_PRODUCTS_PATH = 'catalogs/products';
  protected $url;

  public function __construct($rootUrl, $accessToken) {
    $this->url = $rootUrl.self::PAYPAL_PRODUCTS_PATH;
    $this->accessToken = $accessToken;
  }
}
