<?php

namespace Paypal;

use Paypal\Plan;
use Paypal\Product;
use Paypal\Webhook;
use Paypal\Subscription;
use Paypal\Authentication;

class Paypal
{
  const PRODURL_PAYPAL = 'https://api-m.paypal.com/v1/';
  const SANDBOXURL_PAYPAL = 'https://api-m.sandbox.paypal.com/v1/';

  public string $clientId;
  public string $clientSecret;
  public string $webhookId;
  public bool $isProd;

  public Product $product;
  public Plan $plan;
  public Subscription $subscription;
  public Webhook $webhook;

  public $accessToken;

  public function __construct($clientId, $clientSecret, $isProd)
  {
    $this->clientId = $clientId;
    $this->clientSecret = $clientSecret;
    $this->isProd = $isProd === 'true' ? true : false;
    $this->accessToken = (new Authentication($this->isProd ? self::PRODURL_PAYPAL : self::SANDBOXURL_PAYPAL))->getToken($clientId, $clientSecret);

    $this->product = new Product($this->isProd ? self::PRODURL_PAYPAL : self::SANDBOXURL_PAYPAL, $this->accessToken);

    $this->plan = new Plan($this->isProd ? self::PRODURL_PAYPAL : self::SANDBOXURL_PAYPAL, $this->accessToken);

    $this->subscription = new Subscription($this->isProd ? self::PRODURL_PAYPAL : self::SANDBOXURL_PAYPAL, $this->accessToken);

    $this->webhook = new Webhook($this->isProd ? self::PRODURL_PAYPAL : self::SANDBOXURL_PAYPAL, $this->accessToken);
  }

  public function findAllProduct()
  {
    return $this->product->findAll();
  }

  public function createProduct($product)
  {
    return $this->product->create($product);
  }

  public function findAllPlan()
  {
    return $this->plan->findAll();
  }

  public function createPlan($plan)
  {
    return $this->plan->create($plan);
  }

  public function activatePlan($id, $acticvate)
  {
    return $this->plan->activate($id, $acticvate);
  }

  public function showPlan($id)
  {
    return $this->plan->show($id);
  }

  public function createSubscription($subscription)
  {
    return $this->subscription->create($subscription);
  }

  public function showSubscription($id)
  {
    return $this->subscription->show($id);
  }

  public function cancelSubscription($id)
  {
    return $this->subscription->cancelSubscription($id);
  }


  public function findAllWebhook()
  {
    return $this->webhook->createWebhook();
  }

  public function createWebhook()
  {
    return $this->webhook->findAll();
  }

  public function deleteWebhook($id)
  {
    return $this->webhook->deleteWebhook($id);
  }

  public function handleWebhook($event, $webhookId)
  {
    return $this->webhook->handleWebhook($event, $webhookId);
  }


}
