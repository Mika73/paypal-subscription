<?php

namespace Paypal;

use stdClass;
use Paypal\Api;

class Webhook extends Model
{
  private const PAYPAL_WEBHOOKS_PATH = 'notifications/webhooks';
  private const PAYPAL_WEBHOOKS_VERIRY = 'notifications/verify-webhook-signature';

  protected $url;
  protected $verifyUrl;

  public function __construct($rootUrl, $accessToken)
  {
    $this->url = $rootUrl . self::PAYPAL_WEBHOOKS_PATH;
    $this->verifyUrl = $rootUrl . self::PAYPAL_WEBHOOKS_VERIRY;
    $this->accessToken = $accessToken;
  }

  public function createWebhook()
  {
    $data = new stdClass();
    $data->event_types = [];

    $location = $_SERVER['HTTP_HOST'];
    if (substr($location, 0, 9) === 'localhost') {
      $data->url = "http://$location/createSubscription.php?webhook=true";
    } else {
      $data->url = "https://$location/createSubscription.php?webhook=true";
    }

    $event_type = new stdClass();
    $event_type->name = 'BILLING.SUBSCRIPTION.CREATED';
    $data->event_types[] = $event_type;

    $event_type = new stdClass();
    $event_type->name = 'PAYMENT.SALE.COMPLETED';
    $data->event_types[] = $event_type;

    $event_type = new stdClass();
    $event_type->name = 'BILLING.SUBSCRIPTION.CANCELLED';
    $data->event_types[] = $event_type;

    $event_type = new stdClass();
    $event_type->name = 'BILLING.SUBSCRIPTION.PAYMENT.FAILED';
    $data->event_types[] = $event_type;

    $resp = Api::callApi($this->accessToken, $this->url, 'POST', json_encode($data));

    return $resp;
  }

  public function deleteWebhook($id)
  {
    $url = "$this->webhookUrl/{$id}";
    Api::callApi($this->accessToken, $url, 'DELETE', null);
  }

  public function handleWebhook($event, $webhookId)
  {
    $resp = $this->verifyWebhookSignature($event, $webhookId);

    if ($resp->verification_status == 'SUCCESS') {

      if ($event->event_type === 'BILLING.SUBSCRIPTION.CREATED') {
        $links = $event->resource->links;
        $url = '';
        foreach ($links as $link) {
          if ($link->rel === 'approve') {
            $url = $link->href;
            break;
          }
        }
        Api::callApi($this->accessToken, $url, 'POST', null);
        return $event->event_type;
      }

      return $event->event_type;
    }
  }

  private function verifyWebhookSignature($event, $webhookId)
  {
    $data = json_encode([
      'auth_algo' => $_SERVER['HTTP_PAYPAL_AUTH_ALGO'],
      'cert_url' => $_SERVER['HTTP_PAYPAL_CERT_URL'],
      'transmission_id' => $_SERVER['HTTP_PAYPAL_TRANSMISSION_ID'],
      'transmission_sig' => $_SERVER['HTTP_PAYPAL_TRANSMISSION_SIG'],
      'transmission_time' => $_SERVER['HTTP_PAYPAL_TRANSMISSION_TIME'],
      'webhook_event' => $event,
      'webhook_id' => $webhookId
    ]);
    $resp = Api::callApi($this->accessToken, $this->verifyUrl, 'POST', $data);
    return $resp;
  }
}
