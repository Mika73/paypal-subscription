<?php

namespace Paypal;

use stdClass;
// use Service\Db;
use Paypal\Api;

class Subscription extends Model
{
  private const PAYPAL_SUBSCRIPTIONS_PATH = 'billing/subscriptions';

  protected $url;

  public function __construct($rootUrl, $accessToken)
  {
    $this->url = $rootUrl . self::PAYPAL_SUBSCRIPTIONS_PATH;
    $this->accessToken = $accessToken;
  }

  public function cancelSubscription($id)
  {
    $data = new stdClass();
    $url = "$this->url/$id/cancel";
    //TODO let clients to choose a reason
    $data->reason = 'Not satisfied with the service';

    $resp = Api::callApi($this->accessToken, $url, 'POST', json_encode($data));

    $resp = $this->show($id);
    return $resp;
  }
}
