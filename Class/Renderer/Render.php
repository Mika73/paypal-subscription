<?php

namespace Renderer;
class Render
{
  public static function header()
  {
    return '<!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paypal TEST</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  </head>';
  }

  public static function flashMessage()
  {
    if (isset($_SESSION['flash_message'])) {
      $message = $_SESSION['flash_message'];
      unset($_SESSION['flash_message']);
      echo "<p class='bg-success text-white p-2'>$message</p>";
    }
    if (isset($_SESSION['flash_err_message'])) {
      $message = $_SESSION['flash_err_message'];
      unset($_SESSION['flash_err_message']);
      echo "<p class='bg-danger text-white p-2'>$message</p>";
    }
  }
}
