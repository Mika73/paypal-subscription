<?php

namespace Service;

use PDO;
use PDOException;

class Db
{
  public static $pdo = null;

  public function __construct()
  {
    if (self::$pdo === null) {
      $dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();

      $database = $dotEnv['DATABASE_DNS'];
      $databaseUser = $dotEnv['DATABASE_USER'];
      $databasePassword = $dotEnv['DATABASE_PASSWORD'];

      try {
        $db = new PDO($database, $databaseUser, $databasePassword);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      } catch (PDOException $e) {
        echo "Connection failed : " . $e->getMessage();
      }
      self::$pdo = $db;
    }
  }

  public function createSubscription($subscriptionId, $status)
  {
    try {
      $query = 'INSERT INTO Subscription SET subscription_id = :subscription_id, status = :status';
      $stmt = self::$pdo->prepare($query);
      $stmt->bindParam(':subscription_id', $subscriptionId, PDO::PARAM_STR);
      $stmt->bindParam(':status', $status, PDO::PARAM_STR);
      $stmt->execute();
    } catch (PDOException $e) {
      echo "Error : " . $e->getMessage();
    }
  }

  public function updateSubscription($subscriptionId, $status, $resource)
  {
    try {
      $query = 'UPDATE Subscription SET status = :status , resource = :resource WHERE subscription_id = :subscription_id';
      $stmt = self::$pdo->prepare($query);
      $stmt->bindParam(':status', $status, PDO::PARAM_STR);
      $stmt->bindParam(':resource', $resource, PDO::PARAM_STR);
      $stmt->bindParam(':subscription_id', $subscriptionId, PDO::PARAM_STR);
      $stmt->execute();

    } catch (PDOException $e) {
      echo "Error : " . $e->getMessage();
    }
  }

  public function findAllSubscription()
  {
    try {
      $query = 'SELECT * FROM Subscription';
      $stmt = self::$pdo->prepare($query);
      $stmt->execute();
      $list = $stmt->fetchAll(PDO::FETCH_ASSOC);

    } catch (PDOException $e) {
      echo "Error : " . $e->getMessage();
    }
    return $list;
  }
}
