<?php

use Service\Db;
use Paypal\Paypal;
use Service\DotEnv;
use Renderer\Render;

require_once 'Class/autoload.php';

$dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
$paypal = new Paypal($dotEnv['CLIENT_ID'], $dotEnv['CLIENT_SECRET'], $dotEnv['IS_PROD']);

$productList = $paypal->findAllProduct();
$planList = $paypal->findAllPlan();
$subscriptionList = (new Db())->findAllSubscription();

// Activate or deactivate a plan
if (isset($_GET['plan_action']) && isset($_GET['id'])) {
  if ($_GET['plan_action'] === 'activate') {
    $paypal->activatePlan($_GET['id'], true);
  }
  if ($_GET['plan_action'] === 'deactivate') {
    $paypal->activatePlan($_GET['id'], false);
  }
  header('Location: /');
}

// Deactivate a subscription
if (isset($_GET['subscription_action']) && isset($_GET['id'])) {
  if ($_GET['subscription_action'] === 'deactivate') {
    $paypal->cancelSubscription($_GET['id']);
    (new Db())->updateSubscription($_GET['id'], 'CANCEL.REQUEST.SENT', null);
  }
  header('Location: /');
}

// Create webhook
if (isset($_GET['create_webhook']) && $_GET['create_webhook'] === 'true') {

  $resp = $paypal->findAllWebhook();

  if(isset($resp->webhooks)){
    foreach ($resp->webhooks as $webhook) {
      $paypal->deleteWebhook($webhook->id);
    }
  }

  // Delete webhookid of .env
  $dotEnvFile = file('./.env');
  $i = 0;
  $lineNo = 0;
  foreach ($dotEnvFile as $line) {
    if (substr($line, 0, 10) === 'WEBHOOK_ID') {
      $lineNo = $i;
      break;
    }
    $i++;
  }
  unset($dotEnvFile[$lineNo]);
  file_put_contents('./.env', $dotEnvFile);

  $resp = $paypal->createWebhook();
  $webhookId = $resp->webhooks[0]->id;

  // Register webhook ID in .env
  file_put_contents("./.env", "WEBHOOK_ID=$webhookId", FILE_APPEND);
}
?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Paypal TEST</h1>
    <hr>
    <h2 class="mt-5">Product</h2>
    <table class="table table-striped">
      <tr>
        <th>Product ID</th>
        <th>Product Name</th>
        <th>Create at</th>
      </tr>
      <?php
      foreach ($productList->products as $pr) {
        echo "
        <tr>
          <td>$pr->id</td>
          <td>$pr->name</td>
          <td>$pr->create_time</td>
        </tr>";
      }
      ?>
    </table>

    <a href="createProduct.php">Create a new product</a>
    <hr>
    <h2 class="mt-5">Plan</h2>
    <table class="table table-striped">
      <tr>
        <th>Plan ID</th>
        <th>Product ID</th>
        <th>Plan Name</th>
        <th>Status</th>
        <th>Create at</th>
        <th>Action</th>
        <th>Show</th>
      </tr>
      <?php
      foreach ($planList->plans as $pl) {
        echo "
        <tr>
          <td>$pl->id</td>
          <td>$pl->product_id</td>
          <td>$pl->name</td>
          <td>$pl->status</td>
          <td>$pl->create_time</td>
          <td>";
        if ($pl->status === 'ACTIVE') {
          echo "<a href='/?plan_action=deactivate&id=$pl->id'>Deactivate</a></td>";
        } else {
          echo "<a href='/?plan_action=activate&id=$pl->id'>Activate</a></td>";
        }
        echo "<td><a href='show.php?id=$pl->id&data=plan'> Show</a></td></tr>";
      }
      ?>
    </table>

    <a href="createPlan.php">Create a new plan</a><br>

    <hr>
    <h2 class="mt-5">Subscription</h2>
    <table class="table table-striped">
      <tr>
        <th>ID</th>
        <th>Status</th>
        <th>Amount</th>
        <th>Action</th>
        <th>Show</th>
      </tr>
      <?php
      foreach ($subscriptionList as $sub) {
        $resource = isset($sub['resource'])? json_decode($sub['resource']): '';
        $resourceId = isset($sub['subscription_id'])? $sub['subscription_id']: '';
        $amount = isset($resource->amount->total)? $resource->amount->total: '';
        $currency = isset($resource->amount->currency)? $resource->amount->currency: '';
        echo "
        <tr>
          <td>" . $sub['subscription_id'] . "</td>
          <td>" . $sub['status'] . "</td>
          <td>" . $amount .
          " " . $currency .
          "</td>
          <td>";
        switch ($sub['status']) {
          case 'PAYMENT.SALE.COMPLETED':
            echo "<a href='/?subscription_action=deactivate&id=$resourceId'>Deactivate</a></td>";
            break;
          default:
            echo "</td>";
        }
        echo "<td><a href='show.php?id=$resourceId&data=subscription'> Show</a></td></tr>";
      }
      ?>
    </table>

    <div class="mb-5">
      <a href='/?create_webhook=true'>Create webhook</a><br>
      <a href="createSubscription.php">New subscription</a>
    </div>
  </div>
</body>

</html>
