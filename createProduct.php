<?php

use Paypal\Paypal;
use Service\DotEnv;
use Renderer\Render;

require_once 'Class/autoload.php';

if (isset($_POST['submit'])) {
  $product = new stdClass();
  $product->name = htmlspecialchars(trim($_POST["pname"]));
  $product->type = htmlspecialchars(trim($_POST["ptype"]));

  $dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
  $paypal = new Paypal($dotEnv['CLIENT_ID'], $dotEnv['CLIENT_SECRET'], $dotEnv['IS_PROD']);
  $paypal->createProduct($product);

  header('Location: index.php');
}
?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Create Product </h1>

    <form method="post">

      <div class="form-group  mt-5">
        <label for="pname">Product name:</label>
        <input class="form-control" type="text" id="pname" name="pname">
      </div>

      <div class="form-group mt-5">
        <label for="ptype">Product type:</label>
        <select class="form-select" name="ptype" id="ptype">
          <option value="PHYSICAL">PHYSICAL(Physical goods.)</option>
          <option value="DIGITAL">DIGITAL(Digital goods.)</option>
          <option value="SERVICE">SERVICE(A service. For example, technical support.)</option>
        </select>
      </div>

      <input class="btn btn-secondary mt-5" type="submit" name="submit" value="Create">
    </form>
    <div class="my-5">
      <a href="/">Back</a>
    </div>
  </div>
</body>

</html>
