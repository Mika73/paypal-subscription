<?php

use Paypal\Paypal;
use Service\DotEnv;
use Renderer\Render;

require_once 'Class/autoload.php';

$dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
$paypal = new Paypal($dotEnv['CLIENT_ID'], $dotEnv['CLIENT_SECRET'], $dotEnv['IS_PROD']);

if (isset($_GET['id']) && isset($_GET['data'])) {
  if($_GET['data']=== 'subscription'){
    $resp = $paypal->showSubscription($_GET['id']);
  }else{
    $resp = $paypal->showPlan($_GET['id']);
  }
  $json_pretty = json_encode($resp, JSON_PRETTY_PRINT);
}

?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Paypal TEST</h1>
    <hr>
    <h2 class="mt-5"><?= $_GET['data'] ?> details</h2>
    <a href="index.php">Back</a><br><br>
    <pre><?= $json_pretty ?><pre/>
