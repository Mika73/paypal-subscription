CREATE DATABASE IF NOT EXISTS paypal;
USE paypal;
CREATE TABLE IF NOT EXISTS `Subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_id` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `resource` JSON,
  PRIMARY KEY (`id`)
)
