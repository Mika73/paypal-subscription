<?php

use Paypal\Paypal;
use Service\DotEnv;
use Renderer\Render;

require_once 'Class/autoload.php';

$dotEnv = (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();
$paypal = new Paypal($dotEnv['CLIENT_ID'], $dotEnv['CLIENT_SECRET'], $dotEnv['IS_PROD']);

$products = $paypal->findAllProduct();

if (isset($_POST['submit'])) {
  $plan = new stdClass();
  $billing_cycle = new stdClass();

  $plan->product_id = htmlspecialchars(trim($_POST["pid"]));
  $plan->name = htmlspecialchars(trim($_POST["plname"]));

  $billing_cycle->frequency->interval_unit = htmlspecialchars(trim($_POST["interval_unit"]));
  $billing_cycle->frequency->interval_count = intval(htmlspecialchars(trim($_POST["interval_count"])));
  $billing_cycle->tenure_type = 'REGULAR';
  $billing_cycle->sequence = 1;
  $billing_cycle->total_cycles = 0;
  $billing_cycle->pricing_scheme->fixed_price->value = htmlspecialchars(trim($_POST["setup_fee"]));
  $billing_cycle->pricing_scheme->fixed_price->currency_code = htmlspecialchars(trim($_POST["currency_code"]));

  $plan->billing_cycles[] = $billing_cycle;

  $plan->payment_preferences->auto_bill_outstanding = $_POST["auto_bill_outstanding"] == 'yes' ? true : false;
  $plan->payment_preferences->payment_failure_threshold = intval(htmlspecialchars(trim($_POST["payment_failure_threshold"])));

  $plans = $paypal->createPlan($plan);
  header('Location: index.php');
}
?>

<?= Render::header() ?>

<body>
  <div class="container mt-5">
    <h1>Create Plan</h1>

    <form method="post">
      <div class="form-group">
        <label class="h5" for="pid">Product:</label>
        <select class="form-select" name="pid" id="pid">
          <?php
          foreach ($products->products as $product) {
            echo " <option value='$product->id'>$product->name</option>";
          }
          ?>
        </select>
      </div>

      <div class="form-group mt-5">
        <label class="h5" for="plname">Plan name</label>
        <input class="form-control" type="text" id="plname" name="plname">
      </div>

      <div class="form-group mt-5">
        <label class="h5" for="interval_unit">Interval at which the subscription is charged or billed</label>
        <select class="form-select" name="interval_unit" id="interval_unit">
          <option value="DAY">DAY ( A daily billing cycle )</option>
          <option value="WEEK">WEEK ( A weekly billing cycle )</option>
          <option value="MONTH">MONTH ( A monthly billing cycle )</option>
          <option value="YEAR">YEAR ( A yearly billing cycle )</option>
        </select>
      </div>

      <div class="form-group mt-5">
        <label class="h5" for="interval_count">Number of intervals after which a subscriber is billed</label>
        <input class="form-control" type="number" id="interval_count" name="interval_count">
        <p><small class="text-success">For example, if the interval_unit is DAY with an interval_count of 2, the subscription is billed once every two days.</small></p>
      </div>

      <div class="form-group mt-5">
        <label class="h5" for="setup_fee">Price</label>
        <input class="form-control" type="number" id="setup_fee" name="setup_fee">
      </div>

      <div class="form-group mt-5">
        <label class="h5" for="currency_code">Currency</label>
        <input class="form-control" type="text" id="currency_code" name="currency_code">
      </div>

      <div class="form-group mt-5">
        <label class="h5" for="payment_failure_threshold">Maximum number of payment failures before a subscription is suspended</label>
        <input class="form-control" type="number" id="payment_failure_threshold" name="payment_failure_threshold">
        <p><small class="text-success">Maximum value: 999</small></p>
      </div>

      <div class="form-group mt-5">
        <label class="h5" for="setup_fee_failure_action">Action to take on the subscription if the initial payment for the setup fails</label>
        <select class="form-select" name="setup_fee_failure_action" id="setup_fee_failure_action">
          <option value="CONTINUE">CONTINUE ( Continues the subscription if the initial payment for the setup fails )</option>
          <option value="CANCEL">CANCEL ( Cancels the subscription if the initial payment for the setup fails )</option>
        </select>
      </div>

      <div class="form-check mt-5">
        <input class="form-check-input" type="checkbox" id="auto_bill_outstanding" name="auto_bill_outstanding" value="yes">
        <label class="h5" class="form-check-label" for="auto_bill_outstanding">Automatically bill the outstanding amount in the next billing cycle</label>
      </div>

      <input class="btn btn-secondary mt-5" type="submit" name="submit" value="Create Plan">
    </form>
    <div class="my-5">
      <a href="/">Back</a>
    </div>
  </div>
</body>

</html>
