# Paypal subscription
This app is for building PayPal continuous payments.
[Paypal subscription](https://developer.paypal.com/docs/subscriptions/)
## Requirement
- PHP >= 7.4
- MySQL 8.0
- Paypal account
## How to set up the application
1. Create database
Execute the following command under the root directory
```
mysql -u username -p < paypal.sql
```
2. Create .env file
```
DATABASE_DNS=mysql:host=localhost;dbname=paypal;charset=utf8mb4
DATABASE_USER= //Your database user name
DATABASE_PASSWORD= //Your database password
CLIENT_ID= //Paypal client id
CLIENT_SECRET= //Paypal secret
IS_PROD= //false or true
WEBHOOK_ID= //Leave blank if entering from application
```
3. Create webhook
- From the top page, click the create webhook button to register the webhook required for the subscription.
## Local test
[ngrok](https://ngrok.com/) is useful for testing webhooks in a local environment
```
ngrok http http://localhost:3000
```
